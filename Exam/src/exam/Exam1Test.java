package exam;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Exam1Test {

	@Test
	void test() {
		boolean result;
		Exam1 exam = new Exam1();
		
		result = exam.exam(3);
		
		assertEquals(true, result);
	}

}
